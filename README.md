# MODBUS Libraries

Supporting MODBUS communication and control of WATLOW Easy Zone devices.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)
* [About Modbus](#About-Modbus)
* [About EZ-Zone](#About-EZ-Zone)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Automata Libraries] - State machine lbraries
* [Enums Libraries] - .NET Standard libraries
* [Json Libraries] - .NET Standard libraries
* [Logging Libraries] - .NET Standard libraries
* [Std Libraries] - .NET Standard libraries
* [Tracing Libraries] - .NET Standard libraries
* [Modbus Libraries] - Automata Libraries
* [Units Amounts] - Units and amounts Libraries
* [Modbus Libraries] - MODBUS Libraries
* [IDE Repository] - IDE support files.

```
git clone git@bitbucket.org:davidhary/dn.automata.git
git clone git@bitbucket.org:davidhary/dn.enums.git
git clone git@bitbucket.org:davidhary/dn.json.git
git clone git@bitbucket.org:davidhary/dn.logging.git
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.tracing.git
git clone git@bitbucket.org:davidhary/dn.automata.git
git clone git@bitbucket.org:davidhary/Arebis.UnitsAmounts
git clone git@bitbucket.org:davidhary/dn.modbus.git
git clone git@bitbucket.org:davidhary/vs.ide.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%dnlib%\core\enums
%dnlib%\core\json
%dnlib%\core\logging
%dnlib%\core\std
%dnlib%\core\tracing
%dnlib%\core\units.amounts
%dnlib%\algorithms\automata
%dnlib%\io\modbus
%vslib%\core\ide
```
where %dnlib% and %vslib% are  the root folders of the .NET libraries, e.g., %my%\lib\vs 
and %my%\libraries\vs, respectively, and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

#### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Funduc Search and Replace]
* [Free .Net MODBUS] - MODBUS Library for .NET

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix] -- we are but a spec on the shoulders of giants  
* [John Simmons] - outlaw programmer  
* [Stack overflow] - Joel Spolsky  
* [.Net Foundation] - The .NET Foundation

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Std Libraries]  
[Units Amounts]  
[Automata Libraries]  
[Free .Net MODBUS]  

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Std Libraries]  
[Automata Libraries]  

<a name="About-Modbus"></a>
### About Modbus
For additional information about the MODBUS protocol, see:  
[Simply Modbus]  
[BB-Electronics FAQ]  

<a name="About-EZ-Zone"></a>
### About EZ-Zone
Additional information about EZ-Zone is available at the following sites:  
[Watlow resources and support]  

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Enums Libraries]: https://bitbucket.org/davidhary/dn.enums
[Json Libraries]: https://bitbucket.org/davidhary/dn.json
[Logging Libraries]: https://bitbucket.org/davidhary/dn.logging
[Std Libraries]: https://bitbucket.org/davidhary/dn.std
[Tracing Libraries]: https://bitbucket.org/davidhary/dn.tracing`
[Units Amounts]: https://bitbucket.org/davidhary/Arebis.UnitsAmounts
[Modbus Libraries]: https://bitbucket.org/davidhary/dn.modbus
[Automata Libraries]: https://bitbucket.org/davidhary/dn.automata
[Free .Net MODBUS]: https://code.google.com/p/free-dotnet-modbus

[Microsoft /.NET Framework]: https://dotnet.microsoft.com/download

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[WiX Repository]: https://www.bitbucket.org/davidhary/vs.wix

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
[.Net Foundation]: https://source.dot.net

[Simply Modbus]: http://www.simplymodbus.ca/FAQ.htm
[BB-Electronics FAQ]: http://www.bb-elec.com/Learning-Center/All-White-Papers/Modbus/The-Answer-to-the-14-Most-Frequently-Asked-Modbus.aspx
[Watlow resources and support]: https://www.watlow.com/resources-and-support




