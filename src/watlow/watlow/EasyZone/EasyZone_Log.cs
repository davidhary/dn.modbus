using System;

using isr.Logging.TraceLog;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;

namespace isr.Modbus.Watlow
{
    public partial class EasyZone
    {

        /// <summary>   Add exception data. </summary>
        /// <remarks>   David, 2021-07-02. </remarks>
        /// <param name="ex">       The exception. </param>
        /// <returns>   True if exception data was added for this exception; otherwise, false. </returns>
        protected virtual bool AddExceptionData( Exception ex )
        {
            return true;
        }

        /// <summary>   Gets or sets the assembly log level, which filters the message before it is sent to the logger. </summary>
        /// <value> The assembly log level. </value>
        public static LogLevel AssemblyLogLevel { get; set; } = LogLevel.Trace;

        /// <summary>
        /// Writes a trace message with caller information. Contains the most detailed messages. These
        /// messages may contain sensitive application data. These messages are disabled by default and
        /// should never be enabled in a production environment.
        /// </summary>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogTrace( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( LogLevel.Trace, message, EasyZone.AssemblyLogLevel, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>
        /// Writes a verbose (trace for the Microsoft ILogger) message with caller information. Contains
        /// the most detailed messages. These messages may contain sensitive application data. These
        /// messages are disabled by default and should never be enabled in a production environment.
        /// </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogVerbose( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( LogLevel.Trace, message, EasyZone.AssemblyLogLevel, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>
        /// Writes a Debug message with caller information. Used for interactive investigation during
        /// development. These logs should primarily contain information useful for debugging and have no
        /// long-term value.
        /// </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogDebug( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( LogLevel.Debug, message, EasyZone.AssemblyLogLevel, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>   Logs an information. </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogInformation( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( LogLevel.Information, message, EasyZone.AssemblyLogLevel, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>
        /// Writes a Warning message with caller information. Logs that highlight an abnormal or
        /// unexpected event in the application flow, but do not otherwise cause the application
        /// execution to stop.
        /// </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogWarning( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( LogLevel.Warning, message, EasyZone.AssemblyLogLevel, memberName, sourcePath, sourceLineNumber );
        }


        /// <summary>
        /// Writes a Error message with caller information. Logs that highlight when the current flow of
        /// execution is stopped due to a failure. These should indicate a failure in the current
        /// activity, not an application-wide failure.
        /// </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogError( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( LogLevel.Error, message, EasyZone.AssemblyLogLevel, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>
        /// Writes a Error message with caller information. Logs that highlight when the current flow of
        /// execution is stopped due to a failure. These should indicate a failure in the current
        /// activity, not an application-wide failure.
        /// </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="ex">               The exception. </param>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogError( Exception ex, string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogError( ex, message, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>
        /// Writes a Critical (Fatal) Serilog message with caller information. Logs that describe an
        /// unrecoverable application or system crash, or a catastrophic failure that requires immediate
        /// attention.
        /// </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogCritical( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( LogLevel.Critical, message, EasyZone.AssemblyLogLevel, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>
        /// Writes a Fatal (Critical for the Microsoft ILogger) message  with caller information.
        /// Logs that describe an unrecoverable application or system crash, or a catastrophic
        /// failure that requires immediate attention.
        /// </summary>
        /// <remarks>   David, 2021-08-16. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="memberName">       (Optional) Name of the member. </param>
        /// <param name="sourcePath">       (Optional) Full pathname of the source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Source line number. </param>
        /// <returns>   The logged message. </returns>
        public static string LogFatal( string message, [CallerMemberName] string memberName = "",
                                                       [CallerFilePath] string sourcePath = "",
                                                       [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( LogLevel.Critical, message, EasyZone.AssemblyLogLevel, memberName, sourcePath, sourceLineNumber );
        }

        /// <summary>   Logs a message with caller information. </summary>
        /// <remarks>   David, 2021-07-03. </remarks>
        /// <param name="messageLevel">     The message log level. </param>
        /// <param name="message">          The message. </param>
        /// <param name="minimumLogLevel">  The minimum log level. </param>
        /// <param name="memberName">       (Optional) Name of the caller member. </param>
        /// <param name="sourceFilePath">   (Optional) Full pathname of the caller source file. </param>
        /// <param name="sourceLineNumber"> (Optional) Line number in the caller source file. </param>
        /// <returns>   A string. </returns>
        protected static string LogMessage( LogLevel messageLevel, string message, LogLevel minimumLogLevel = LogLevel.Trace,
                                            [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "",
                                            [CallerLineNumber] int sourceLineNumber = 0 )
        {
            return TraceLogger.LogCallerMessage( messageLevel, message, minimumLogLevel, memberName, sourceFilePath, sourceLineNumber );
        }

        /// <summary>   Logger close and flush. </summary>
        /// <remarks>   David, 2021-12-06. </remarks>
        public static void LoggerCloseAndFlush()
        {
            TraceLogger.CloseAndFlush();
        }


    }
}
