# About

isr.Modbus.Watlow is a .Net library supporting WATLOW controllers.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Modbus.Watlow is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Modbus Repository].

[Modbus Repository]: https://bitbucket.org/davidhary/dn.modbus

