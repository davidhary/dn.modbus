using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using isr.Automata.Finite.Engines;
using FastEnums;

using Stateless;

using isr.Modbus.Watlow.WinControls.ErrorProviderExtensions;
using System.Runtime.CompilerServices;

namespace isr.Modbus.Watlow.WinControls
{
    /// <summary> View for accessing the Watlow EZ-Zone controller. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-05-09 </para>
    /// </remarks>
    public partial class EasyZoneView : System.Windows.Forms.Form, INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        private bool InitializingComponents { get; set; }
        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        public EasyZoneView()
        {
            // This call is required by the designer.
            this.InitializingComponents = true;
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._SetpointNumeric.NumericUpDown.DecimalPlaces = 1;
            this._SetpointNumeric.NumericUpDown.Minimum = -200;
            this._SetpointNumeric.NumericUpDown.Maximum = 400m;
            this._SetpointNumeric.NumericUpDown.Value = 78m;

            this._SoakSetpointNumeric.NumericUpDown.DecimalPlaces = 0;
            this._SoakSetpointNumeric.NumericUpDown.Minimum = -200;
            this._SoakSetpointNumeric.NumericUpDown.Maximum = 400m;
            this._SoakSetpointNumeric.NumericUpDown.Value = 90m;

            this._SoakTimeNumeric.NumericUpDown.DecimalPlaces = 0;
            this._SoakTimeNumeric.NumericUpDown.Minimum = 0m;
            this._SoakTimeNumeric.NumericUpDown.Maximum = 3600m;
            this._SoakTimeNumeric.NumericUpDown.Value = 60m;

            this._SoakWindowNumeric.NumericUpDown.DecimalPlaces = 1;
            this._SoakWindowNumeric.NumericUpDown.Increment = 0.1m;
            this._SoakWindowNumeric.NumericUpDown.Minimum = 0m;
            this._SoakWindowNumeric.NumericUpDown.Maximum = 10m;
            this._SoakWindowNumeric.NumericUpDown.Value = 2m;

            this._SoakHysteresisNumeric.NumericUpDown.DecimalPlaces = 1;
            this._SoakHysteresisNumeric.NumericUpDown.Increment = 0.1m;
            this._SoakHysteresisNumeric.NumericUpDown.Minimum = 0m;
            this._SoakHysteresisNumeric.NumericUpDown.Maximum = 10m;
            this._SoakHysteresisNumeric.NumericUpDown.Value = 1m;

            this._SampleIntervalNumeric.NumericUpDown.DecimalPlaces = 0;
            this._SampleIntervalNumeric.NumericUpDown.Minimum = 0m;
            this._SampleIntervalNumeric.NumericUpDown.Maximum = 100m;
            this._SampleIntervalNumeric.NumericUpDown.Value = 2m;

            this._SoakResetDelayNumeric.NumericUpDown.DecimalPlaces = 0;
            this._SoakResetDelayNumeric.NumericUpDown.Minimum = 0m;
            this._SoakResetDelayNumeric.NumericUpDown.Maximum = 600m;
            this._SoakResetDelayNumeric.NumericUpDown.Value = 20m;

            this._RampTimeoutNumeric.NumericUpDown.DecimalPlaces = 0;
            this._RampTimeoutNumeric.NumericUpDown.Minimum = 0m;
            this._RampTimeoutNumeric.NumericUpDown.Maximum = 6000m;
            this._RampTimeoutNumeric.NumericUpDown.Value = 0m;

            this._OvenControlModeComboBox.ComboBox.DataSource = null;
            this._OvenControlModeComboBox.ComboBox.Items.Clear();
            this._OvenControlModeComboBox.ComboBox.DataSource = typeof( OvenControlMode ).ValueDescriptionPairs().ToList();
            this._OvenControlModeComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._OvenControlModeComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );

            this.InitializingComponents = false;
            this._EventNotificationTestSplitButton.Visible = Debugger.IsAttached;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="unitId">   Identifier for the unit. </param>
        /// <param name="portName"> Name of the port. </param>
        public EasyZoneView( int unitId, string portName ) : this()
        {
            this.IsDeviceOwner = true;
            this.AssignDeviceThis( new EasyZone( ( byte ) unitId, portName ) );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="device"> The device. </param>
        public EasyZoneView( EasyZone device ) : this()
        {
            this.IsDeviceOwner = false;
            this.AssignDeviceThis( device );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    this.AssignDevice( null );
                }
                this.components?.Dispose();
                this.components = null;
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " I NOTIFY PROPERTY CHAGNED "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Synchronously notify property changed described by propertyName. </summary>
        /// <remarks>   David, 2021-02-25. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        private isr.Tracing.WinForms.TextBoxTraceEventWriter TextBoxTextWriter { get; set; }

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                this.TextBoxTextWriter = new( this._MessagesBox );
                this.TextBoxTextWriter.ContainerTreeNode = null;
                this.TextBoxTextWriter.ContainerPanel = this._MessagesTabPage;
                this.TextBoxTextWriter.HandleCreatedCheckEnabled = false;
                this.TextBoxTextWriter.TabCaption = "Log";
                this.TextBoxTextWriter.CaptionFormat = "{0} " + Convert.ToChar( 0x1C2 );
                this.TextBoxTextWriter.ResetCount = 1000;
                this.TextBoxTextWriter.PresetCount = 500;
                this.TextBoxTextWriter.TraceLevel = TraceEventType.Information;
                isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.TextBoxTextWriter );

                this._ErrorStatusLabel.Text = string.Empty;
                this._ComplianceToolStripStatusLabel.Visible = false;
                this._TbdToolStripStatusLabel.Visible = false;
                this._TransactionElapsedTimeLabel.Text = "0 ms";
                this._PortComboBox.Items.Clear();
                this._PortComboBox.Items.AddRange( System.IO.Ports.SerialPort.GetPortNames() );
                this.OnDeviceOpenChanged( this.EasyZoneClient );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        #endregion

        #region " MODBUS CLIENT "

        /// <summary>
        /// Gets or sets the sentinel indicating if this panel owns the device and, therefore, needs to
        /// dispose of this device.
        /// </summary>
        /// <value> The is device owner. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool IsDeviceOwner { get; set; }

        /// <summary> Assign device. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        [MethodImpl( MethodImplOptions.Synchronized )]
        private void AssignDeviceThis( EasyZone value )
        {
            if ( this.EasyZoneClient is object )
            {
                this.EasyZoneClient.PropertyChanged -= this.EasyZoneClient_PropertyChanged;
                if ( this.IsDeviceOwner )
                {
                    this.EasyZoneClient.Dispose();
                }
                this.AssignSoakEngine( null );
                this.EasyZoneClient = null;
            }
            this.EasyZoneClient = value;
            if ( value is object )
            {
                this.EasyZoneClient.PropertyChanged += this.EasyZoneClient_PropertyChanged;
                this.OnPropertyChanged( value, nameof( EasyZone.IsDeviceOpen ) );
                this.OnPropertyChanged( value, nameof( EasyZone.InputErrorRead ) );
                this.OnPropertyChanged( value, nameof( EasyZone.AlarmTypeRead ) );
                this.OnPropertyChanged( value, nameof( EasyZone.AlarmTypeWritten ) );
                this.OnPropertyChanged( value, nameof( EasyZone.AnalogInputRead ) );
                this.OnPropertyChanged( value, nameof( EasyZone.LastInterfaceError ) );
                this.OnPropertyChanged( value, nameof( EasyZone.SetpointRead ) );
                this.OnPropertyChanged( value, nameof( EasyZone.SetpointWritten ) );
                this.OnPropertyChanged( value, nameof( EasyZone.Title ) );
                this.OnPropertyChanged( value, nameof( EasyZone.TransactionElapsedTime ) );
                this.AssignSoakEngine( value.SoakEngine );
            }
            this.OnDeviceOpenChanged( value );
        }

        /// <summary> Assign device. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public virtual void AssignDevice( EasyZone value )
        {
            this.IsDeviceOwner = false;
            this.AssignDeviceThis( value );
        }

        /// <summary> Gets or sets the easy zone client. </summary>
        /// <value> The easy zone client. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private EasyZone EasyZoneClient { get; set; }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( EasyZone sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( EasyZone.IsDeviceOpen ):
                    {
                        this.OnDeviceOpenChanged( sender );
                        break;
                    }
                case nameof( EasyZone.InputErrorRead ):
                    {
                        this._InputErrorLabel.Text = EasyZone.BuildCompoundInputError( sender.InputErrorRead );
                        this._InputErrorLabel.ForeColor = sender.InputErrorRead == InputError.NoError ? System.Drawing.Color.Green : System.Drawing.Color.Red;
                        break;
                    }
                case nameof( EasyZone.AlarmTypeRead ):
                    {
                        this._AlarmTypeComboBox.Text = sender.AlarmTypeRead.Description();
                        break;
                    }
                case nameof( EasyZone.AlarmTypeWritten ):
                    {
                        this._AlarmTypeComboBox.Text = sender.AlarmTypeWritten.Description();
                        break;
                    }
                case nameof( EasyZone.AnalogInputRead ):
                    {
                        this._AnalogInputReadingLabel.Text = sender.AnalogInputRead is null ? string.Empty : $"{sender.AnalogInputRead.Value:0.0} {sender.AnalogInputRead.Unit}";
                        break;
                    }
                case nameof( EasyZone.LastInterfaceError ):
                    {
                        this._LastErrorTextBox.Text = ModbusBase.BuildCompoundInterfaceError( sender.LastInterfaceError );
                        this._LastErrorTextBox.ForeColor = sender.LastInterfaceError == InterfaceError.NoError ? System.Drawing.Color.Green : System.Drawing.Color.Red;
                        break;
                    }
                case nameof( EasyZone.SetpointRead ):
                    {
                        this._SetpointLabel.Text = sender.SetpointRead is null ? string.Empty : $"{sender.SetpointRead.Value:0.0} {sender.SetpointRead.Unit}";
                        break;
                    }
                case nameof( EasyZone.SetpointWritten ):
                    {
                        break;
                    }
                case nameof( EasyZone.Title ):
                    {
                        this._TitleLabel.Text = sender.Title;
                        break;
                    }
                case nameof( EasyZone.TransactionElapsedTime ):
                    {
                        this._TransactionElapsedTimeLabel.Text = $"{sender.TransactionElapsedTime.Value:0} {sender.TransactionElapsedTime.Unit}";
                        break;
                    }
            }
        }

        /// <summary> Easy zone client property changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void EasyZoneClient_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            if ( sender is not EasyZone ez || e is null )
                return;
            activity = $"handler {nameof( EasyZone )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.EasyZoneClient_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as EasyZone, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = EasyZone.LogError( ex, activity );
            }
        }

        /// <summary> Applies the alarm type menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031: Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyAlarmTypeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                _ = this._AlarmTypeComboBox.SelectedIndex == 0
                    ? this.EasyZoneClient.ApplyAlarmType( AlarmType.Off )
                    : this.EasyZoneClient.ApplyAlarmType( AlarmType.Process );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, "Exception applying alarm type" );
                _ = EasyZone.LogError( ex, "Exception applying alarm type;. " );
            }
        }

        /// <summary> Reads alarm type menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadAlarmTypeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ReadAlarmType();
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, "Exception reading alarm type" );
                _ = EasyZone.LogError( ex, "Exception reading alarm type;. " );
            }
        }

        /// <summary> Writes an alarm type menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void WriteAlarmTypeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                _ = this._AlarmTypeComboBox.SelectedIndex == 0
                    ? this.EasyZoneClient.WriteAlarmType( AlarmType.Off )
                    : this.EasyZoneClient.WriteAlarmType( AlarmType.Process );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, "Exception writing alarm type" );
                _ = EasyZone.LogError( ex, "Exception writing alarm type;. " );
            }
        }

        /// <summary> Reads input button click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadInputButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ReadAnalogInput();
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, "Exception reading analog input" );
                _ = EasyZone.LogError( ex, "Exception reading analog input;. " );
            }
        }

        /// <summary> Reads input error button click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadInputErrorButton_Click( object sender, EventArgs e )
        {
            string activity = "reading analog input";
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ReadInputError();
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Exception {activity};. " );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
        }

        /// <summary> Applies the setpoint menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySetpointMenuItem_Click( object sender, EventArgs e )
        {
            string activity = "applying setpoint";
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ApplySetpoint( new Arebis.UnitsAmounts.Amount( ( double ) this._SetpointNumeric.Value, this.EasyZoneClient.TemperatureUnit ) );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Exception {activity};. " );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
        }

        /// <summary> Reads setpoint menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadSetpointMenuItem_Click( object sender, EventArgs e )
        {
            string activity = "reading setpoint";
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.ReadSetpoint();
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Exception {activity};. " );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
        }

        /// <summary> Writes a setpoint menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void WriteSetpointMenuItem_Click( object sender, EventArgs e )
        {
            string activity = "writing setpoint";
            try
            {
                this._ErrorProvider.Clear();
                _ = this.EasyZoneClient.WriteSetpoint( new Arebis.UnitsAmounts.Amount( ( double ) this._SetpointNumeric.Value, this.EasyZoneClient.TemperatureUnit ) );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Exception {activity};. " );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
        }

        /// <summary> Recursively enable. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="value">   True to show or False to hide the control. </param>
        protected void RecursivelyEnable( Control control, bool value )
        {
            if ( control is object )
            {
                control.Enabled = value;
                if ( control.Controls is object && control.Controls.Count > 0 )
                {
                    foreach ( Control c in control.Controls )
                        this.RecursivelyEnable( c, value );
                }
            }
        }

        /// <summary> Executes the device open changed action. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="value"> Reference tot he <see cref="EasyZone"/> client. </param>
        protected void OnDeviceOpenChanged( EasyZone value )
        {
            bool isOpen = (value?.IsDeviceOpen).GetValueOrDefault( false );
            foreach ( TabPage t in this._Tabs.TabPages )
            {
                if ( !ReferenceEquals( t, this._MessagesTabPage ) )
                {
                    foreach ( Control c in t.Controls )
                        this.RecursivelyEnable( c, isOpen );
                }
            }
            if ( isOpen )
            {
                if ( !string.Equals( this.EasyZoneClient.PortName, this._PortComboBox.Text, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._PortComboBox.Text = this.EasyZoneClient.PortName;
                }
                this._OpenPortButton.BackColor = System.Drawing.Color.LightGreen;
                this._OpenPortButton.Text = "Close";
                this._StatusLabel.Text = $"Open at {this._PortComboBox.Text}";
            }
            else
            {
                this._OpenPortButton.BackColor = System.Drawing.SystemColors.Control;
                this._OpenPortButton.Text = "Open";
                this._StatusLabel.Text = $"Select port and click {this._OpenPortButton.Text}";
            }
        }

        /// <summary> Opens port button click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenPortButton_Click( object sender, EventArgs e )
        {
            string activity = "opening Easy Zone";
            try
            {
                this._ErrorProvider.Clear();
                if ( this.EasyZoneClient?.IsDeviceOpen == true )
                {
                    this.EasyZoneClient.CloseModbusClient();
                }
                else
                {
                    if ( this.EasyZoneClient is null )
                    {
                        this.IsDeviceOwner = true;
                        this.AssignDevice( new EasyZone( ( byte ) this._UnitIdNumeric.Value, this._PortComboBox.Text ) );
                    }
                    if ( !string.Equals( this.EasyZoneClient.PortName, this._PortComboBox.Text ) )
                    {
                        this.EasyZoneClient.PortName = this._PortComboBox.Text;
                    }
                    if ( this.EasyZoneClient.UnitId != ( byte ) this._UnitIdNumeric.Value )
                    {
                        this.EasyZoneClient.UnitId = ( byte ) this._UnitIdNumeric.Value;
                    }
                    _ = this.EasyZoneClient.TryOpenModbusClient()
                        ? EasyZone.LogVerbose( $"Watlow unit {this.EasyZoneClient.UnitId} open on port {this.EasyZoneClient.PortName};. " )
                        : EasyZone.LogWarning( $"Failed {activity};. Details: {this.EasyZoneClient.LastInterfaceError.Description()}" );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Exception {activity};. " );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
            finally
            {
                this.OnDeviceOpenChanged( this.EasyZoneClient );
            }
        }

        /// <summary> Refresh ports button click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RefreshPortsButton_Click( object sender, EventArgs e )
        {
            string activity = "refreshing list of ports";
            try
            {
                this._ErrorProvider.Clear();
                this._PortComboBox.Items.Clear();
                this._PortComboBox.Items.AddRange( System.IO.Ports.SerialPort.GetPortNames() );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Exception {activity};. " );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
        }

        #endregion

        #region " MODBUS CLIENT: SOAK "

        /// <summary> Gets or sets the Soak engine. </summary>
        /// <value> The Soak engine. </value>
        public SoakEngine SoakEngine { get; private set; }

        /// <summary> Assign Soak Engine. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="value"> The value. </param>
        [MethodImpl( MethodImplOptions.Synchronized )]
        private void AssignSoakEngine( SoakEngine value )
        {
            if ( this.SoakEngine is object )
            {
                this.SoakEngine.PropertyChanged -= this.SoakEngine_PropertyChanged;
                this.SoakEngine = null;
            }
            this.SoakEngine = value;
            if ( value is object )
            {
                this.SoakEngine.PropertyChanged += this.SoakEngine_PropertyChanged;
                this.SoakEngine.UsingFireAsync = true;
                this.SoakEngine.StateMachine.OnTransitionedAsync( ( t ) => Task.Run( () => this.OnTransitioned( t ) ) );
                this.HandlePropertyChanged( value, nameof( this.SoakEngine.SoakPercentProgress ) );
                this.HandlePropertyChanged( value, nameof( this.SoakEngine.StateProgress ) );
            }
        }

        /// <summary> Handles the state entry actions. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="transition"> The transition. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnTransitioned( StateMachine<SoakState, SoakTrigger>.Transition transition )
        {
            string activity = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<StateMachine<SoakState, SoakTrigger>.Transition>( this.OnTransitioned ), new object[] { transition } );
                }
                else if ( transition is null )
                {
                    activity = EasyZone.LogVerbose( $"Entering the {this.SoakEngine.StateMachine.State} state" );
                    this.OnActivated( this.SoakEngine.StateMachine.State );
                }
                else
                {
                    activity = EasyZone.LogVerbose( $"transitioning {this.SoakEngine.Name}: {transition.Source} --> {transition.Destination}" );
                    if ( transition.IsReentry )
                    {
                        this.OnReentry( transition.Destination );
                    }
                    else
                    {
                        this.OnExit( transition.Source );
                        this.OnActivated( transition.Destination );
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = EasyZone.LogError( ex, activity );
            }
        }

        /// <summary> Executes the 'exit' action. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="source"> Source for the. </param>
        private void OnExit( SoakState source )
        {
            switch ( source )
            {
                case SoakState.Soak:
                    {
                        break;
                    }
            }
        }

        /// <summary>
        /// Executes the 'reentry' action. Is used to update the setpoint information and take new
        /// readings.
        /// </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="destination"> Destination for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void OnReentry( SoakState destination )
        {
            string activity = $"{this.SoakEngine.Name} reentered {destination.Description()} state";
            try
            {
                switch ( destination )
                {
                    case SoakState.Soak:
                        {
                            break;
                        }
                    case SoakState.AtTemp:
                        {
                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"info {ex.Data?.Count}", $"{this.SoakEngine.Name} exception at Reenter {destination.Description()} state" );
                _ = EasyZone.LogError( ex, activity );
            }
        }

        /// <summary> Executes the 'activated' action. </summary>
        /// <remarks> David, 2020-11-14. </remarks>
        /// <param name="destination"> Destination for the. </param>
        private void OnActivated( SoakState destination )
        {
            switch ( destination )
            {
                case SoakState.Idle:
                    {
                        this.OnStopped();
                        break;
                    }
                case SoakState.Engaged:
                    {
                        this.OnEngaged();
                        break;
                    }
                case SoakState.Terminal:
                    {
                        this.OnStopped();
                        break;
                    }
            }
        }

        /// <summary> Executes the 'engaged' actions. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        private void OnEngaged()
        {
            this._AlarmToolStrip.Enabled = false;
            this._InputToolStrip.Enabled = false;
            this._ProcessToolStrip.Enabled = false;
            this._AbortSoakMenuItem.Enabled = true;
            this._RefreshSoakMenuItem.Enabled = true;
            this._PauseSoakMenuItem.Enabled = false;
            this._PauseSoakMenuItem.Invalidate();
            this._PauseSoakMenuItem.Checked = false;
            this._PauseSoakMenuItem.Enabled = true;
        }

        /// <summary> Executes the 'stopped' action. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        private void OnStopped()
        {
            this._AlarmToolStrip.Enabled = true;
            this._InputToolStrip.Enabled = true;
            this._ProcessToolStrip.Enabled = true;
            this._AbortSoakMenuItem.Enabled = false;
            this._RefreshSoakMenuItem.Enabled = false;
            this._PauseSoakMenuItem.Enabled = false;
            this._PauseSoakMenuItem.Invalidate();
            this._PauseSoakMenuItem.Checked = false;
            this._PauseSoakMenuItem.Enabled = false;
        }

        /// <summary> Handles the Soak Engine property changed event. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SoakEngine sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Automata.Finite.Engines.SoakEngine.SoakPercentProgress ):
                    {
                        this._SoakElapsedTimeLabel.Text = $"Soak: {sender.SoakStopwatch.Elapsed:d\\.h\\:mm\\:ss\\.f}";
                        break;
                    }
                case nameof( Automata.Finite.Engines.SoakEngine.StateProgress ):
                    {
                        this._SoakStateLabel.Text = sender.CurrentState.Description();
                        if ( sender.CurrentState == SoakState.Soak )
                        {
                            this._SoakElapsedTimeLabel.Text = $"Soak: {sender.SoakStopwatch.Elapsed:d\\.h\\:mm\\:ss\\.f}";
                            this._SoakAutomatonProgress.Value = sender.SoakPercentProgress;
                            this._SoakAutomatonProgress.ToolTipText = "Soak time progress";
                        }
                        else
                        {
                            this._SoakAutomatonProgress.Value = sender.StateProgress;
                            this._SoakAutomatonProgress.ToolTipText = "Soak automaton progress";
                        }
                        _ = EasyZone.LogVerbose( $"{sender.Name} soak progress {sender.SoakStopwatch.Elapsed}" );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by SoakEngine for property changed events. </summary>
        /// <remarks> David, 2020-11-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SoakEngine_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is not SoakEngine automaton || e is null )
                return;
            string activity = $"handling {nameof( Automata.Finite.Engines.SoakEngine )}.{e.PropertyName} change at ({automaton.CurrentState} state)";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SoakEngine_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( automaton, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
        }

        #endregion

        #region " CONTROL EVETN HANDLERS "

        /// <summary> Builds setpoint information. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <returns> A SetpointInfo. </returns>
        private SetpointInfo BuildSetpointInfo()
        {
            return new SetpointInfo( Arebis.StandardUnits.TemperatureUnits.DegreeFahrenheit, ( double ) this._SoakSetpointNumeric.Value, TimeSpan.FromSeconds( ( double ) this._SampleIntervalNumeric.Value ), TimeSpan.FromSeconds( ( double ) this._SoakTimeNumeric.Value ), ( double ) this._SoakWindowNumeric.Value, ( double ) this._SoakHysteresisNumeric.Value, TimeSpan.FromSeconds( ( double ) this._SoakResetDelayNumeric.Value ), ( OvenControlMode ) Convert.ToInt32( this._OvenControlModeComboBox.ComboBox.SelectedValue ), TimeSpan.FromSeconds( ( double ) this._RampTimeoutNumeric.Value ) );
        }

        /// <summary> Validates the oven control mode described by item. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="item"> The item. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private bool ValidateOvenControlMode( ToolStripItem item )
        {
            bool result = ( OvenControlMode ) Convert.ToInt32( this._OvenControlModeComboBox.ComboBox.SelectedValue ) != OvenControlMode.None;
            if ( !result )
            {
                _ = this._ErrorProvider.Annunciate( item, "Select oven control mode" );
                _ = EasyZone.LogInformation( "Oven control mode not selected" );
            }
            return result;
        }

        /// <summary> Soak setpoint numeric value changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SoakSetpointNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.EasyZoneClient.CandidateSetpointTemperature = ( double? ) this._SoakSetpointNumeric.Value;
        }

        /// <summary> Oven control mode combo box selected index changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OvenControlModeComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( this.SoakEngine.IsActive() )
            {
                OvenControlMode controlMode = ( OvenControlMode ) Convert.ToInt32( this._OvenControlModeComboBox.ComboBox.SelectedValue );
                if ( controlMode != OvenControlMode.None )
                {
                    this.EasyZoneClient.CandidateOvenControlMode = controlMode;
                }
            }
        }

        /// <summary> Starts soak menu item check state changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StartSoakMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            sender = this._SoakControlButton;
            string activity = "Starting";
            try
            {
                this._ErrorProvider.Clear();
                this._StartSoakMenuItem.Enabled = false;
                if ( this._StartSoakMenuItem.Checked )
                {
                    activity = "starting";
                    if ( this.ValidateOvenControlMode( this._OvenControlModeComboBox ) )
                    {
                        if ( !this.SoakEngine.IsActive() )
                        {
                            activity = $"Starting setpoint {this._SetpointNumeric.Value}";
                            _ = EasyZone.LogInformation( activity );
                            this.EasyZoneClient.ActivateSoakAutomaton( this.BuildSetpointInfo() );
                        }
                    }
                }
                else
                {
                    activity = $"stopping; Enqueueing a {SoakTrigger.Terminate} signal";
                    if ( this.SoakEngine.IsActive() )
                    {
                        _ = EasyZone.LogVerbose( activity );
                        this.SoakEngine.Terminate();
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Soak Exception {activity}" );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
            finally
            {
                this._StartSoakMenuItem.Text = this._StartSoakMenuItem.Checked ? "Stop" : "Start";
                this._StartSoakMenuItem.Enabled = true;
            }
        }

        /// <summary> Abort soak menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AbortSoakMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            sender = this._SoakControlButton;
            string activity = "issuing a stop request";
            try
            {
                this._ErrorProvider.Clear();
                this.SoakEngine.StopRequested = true;
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Soak Exception {activity}" );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
            finally
            {
            }
        }

        /// <summary> Pause soak menu item check state changed. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PauseSoakMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            sender = this._SoakControlButton;
            string activity = "pausing";
            bool wasEnabled = this._PauseSoakMenuItem.Enabled;
            try
            {
                this._ErrorProvider.Clear();
                if ( this._PauseSoakMenuItem.Enabled )
                {
                    this._PauseSoakMenuItem.Enabled = false;
                    this._PauseSoakMenuItem.Invalidate();
                    if ( this._PauseSoakMenuItem.Checked )
                    {
                        activity = "pausing";
                        this.EasyZoneClient.PauseSoakAutomaton();
                    }
                    else
                    {
                        activity = "resuming";
                        this.EasyZoneClient.ResumeSoakAutomaton();
                    }
                    _ = EasyZone.LogInformation( activity );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Soak Exception {activity}" );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
            finally
            {
                this._PauseSoakMenuItem.Text = this._PauseSoakMenuItem.Checked ? "Resume" : "Pause";
                this._PauseSoakMenuItem.Enabled = wasEnabled;
                this._PauseSoakMenuItem.Invalidate();
            }
        }

        /// <summary> Refresh soak menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RefreshSoakMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            sender = this._SoakControlButton;
            string activity = "refreshing";
            try
            {
                this._ErrorProvider.Clear();
                this._RefreshSoakMenuItem.Enabled = false;
                if ( this.ValidateOvenControlMode( this._OvenControlModeComboBox ) )
                {
                    activity = $"Changing to setpoint {this._SetpointNumeric.Value}";
                    _ = EasyZone.LogInformation( $"Changing to setpoint {this._SetpointNumeric.Value}" );
                    this.EasyZoneClient.ChangeTargetAutomatonSetpoint( this.BuildSetpointInfo(), false );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, $"Soak Exception {activity}" );
                _ = EasyZone.LogError( ex, $"Exception {activity};. " );
            }
            finally
            {
                this._RefreshSoakMenuItem.Enabled = true;
            }
        }

        #endregion

        #region " INVOKE TESTERS "

        /// <summary> The event time builder. </summary>
        private System.Text.StringBuilder _EventTimeBuilder;

        /// <summary> The event stopwatch. </summary>
        private Stopwatch _EventStopwatch;

        /// <summary> Synchronizes the notify menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SyncNotifyMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.EasyZoneClient.HandlerExceptionOccurred += this.EasyZoneClient.RegisterHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred += this.HandleHandlerExceptionOccurred;
                this._EventTimeBuilder = new System.Text.StringBuilder();
                this._EventStopwatch = Stopwatch.StartNew();
                // Me.EasyZoneClient.BeginInvokeHandlerException()
                this.EasyZoneClient.SyncNotifyHandlerException();
                _ = this._EventTimeBuilder.AppendFormat( $">> {this._EventStopwatch.ElapsedMilliseconds:G5}ms" );
                this._TransactionElapsedTimeLabel.Text = this._EventTimeBuilder.ToString();
            }
            finally
            {
                this.EasyZoneClient.HandlerExceptionOccurred -= this.HandleHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred -= this.EasyZoneClient.RegisterHandlerExceptionOccurred;
            }
        }

        /// <summary> Unsafe invoke menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void UnsafeInvokeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.EasyZoneClient.HandlerExceptionOccurred += this.EasyZoneClient.RegisterHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred += this.HandleHandlerExceptionOccurred;
                this._EventTimeBuilder = new System.Text.StringBuilder();
                this._EventStopwatch = Stopwatch.StartNew();
                this.EasyZoneClient.SyncNotifyHandlerException();
                _ = this._EventTimeBuilder.AppendFormat( $" >>{this._EventStopwatch.ElapsedMilliseconds:G5}ms " );
                this._TransactionElapsedTimeLabel.Text = this._EventTimeBuilder.ToString();
            }
            finally
            {
                this.EasyZoneClient.HandlerExceptionOccurred -= this.HandleHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred -= this.EasyZoneClient.RegisterHandlerExceptionOccurred;
            }
        }

        /// <summary> Asynchronous notify menu item click. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void AsyncNotifyMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                this.EasyZoneClient.HandlerExceptionOccurred += this.EasyZoneClient.RegisterHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred += this.HandleHandlerExceptionOccurred;
                this._EventTimeBuilder = new System.Text.StringBuilder();
                this._EventStopwatch = Stopwatch.StartNew();
                this.EasyZoneClient.SyncNotifyHandlerException();
                // Me.EasyZoneClient.SafeBeginInvokeHandlerException()
                _ = this._EventTimeBuilder.AppendFormat( $" >>{this._EventStopwatch.ElapsedMilliseconds:G5}ms " );
                this._TransactionElapsedTimeLabel.Text = this._EventTimeBuilder.ToString();
            }
            finally
            {
                this.EasyZoneClient.HandlerExceptionOccurred -= this.HandleHandlerExceptionOccurred;
                this.EasyZoneClient.HandlerExceptionOccurred -= this.EasyZoneClient.RegisterHandlerExceptionOccurred;
            }
        }

        /// <summary> Handler, called when the handler exception occurred. </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Error event information. </param>
        private void HandleHandlerExceptionOccurred( object sender, ErrorEventArgs e )
        {
            _ = this._EventTimeBuilder.AppendFormat( $" <<{this._EventStopwatch.ElapsedMilliseconds:G5}ms " );
            this._TransactionElapsedTimeLabel.Text = this._EventTimeBuilder.ToString();
            System.Threading.Thread.Sleep( 10 );
        }

        #endregion
    }
}
