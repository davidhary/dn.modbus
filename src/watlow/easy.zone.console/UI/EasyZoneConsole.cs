using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Modbus.Watlow.EasyZone.Console
{
    /// <summary> An easy zone console. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-5-10 </para>
    /// </remarks>
    public partial class EasyZoneConsole : Form
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-11-28. </remarks>
        public EasyZoneConsole()
        {
            this.InitializeComponent();
        }

        #region " CONSTRUCTION "

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );
                // set the form caption
                var assembly = System.Reflection.Assembly.GetExecutingAssembly();
                this.Text = $"{assembly.GetName().Name}.r.{assembly.GetName().Version.ToString(3)}.{this.Name}";
                Application.DoEvents();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", $"Exception loading {this.Name}" );
                _ = isr.Logging.TraceLog.TraceLogger.LogError( ex, "" );
                if ( DialogResult.Abort == MessageBox.Show(ex.ToString(), $"Exception loading {this.Name}",
                                                           MessageBoxButtons.AbortRetryIgnore,MessageBoxIcon.Error, MessageBoxDefaultButton.Button1 ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnLoad( e );
                this.Cursor = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <remarks> David, 2020-11-11. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            try
            {
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );
                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;
                // allow form rendering time to complete: process all messages currently in the queue.
                Application.DoEvents();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", $"Exception showing {this.Name}" );
                if ( DialogResult.Abort == MessageBox.Show( ex.ToString(), $"Exception showing {this.Name}",
                                                           MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1 ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnShown( e );
                this.Cursor = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
            }
        }

        #endregion
    }
}
