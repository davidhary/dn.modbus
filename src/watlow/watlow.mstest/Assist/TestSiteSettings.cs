using System;
using System.Reflection;

using isr.Json;

namespace isr.Modbus.Watlow.MSTest
{

    /// <summary>   A test site settings. </summary>
    /// <remarks>   David, 2021-02-01. </remarks>
    [isr.Json.SettingsSection( nameof( TestSiteSettings ) )]
    public class TestSiteSettings : isr.Json.JsonSettingsBase
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public TestSiteSettings() : base( System.Reflection.Assembly.GetAssembly( typeof( TestSiteSettings ) ) )
        { }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="callingAssembly">  The calling assembly. </param>
        /// <param name="scope">            (Optional) The scope. </param>
        protected TestSiteSettings( Assembly callingAssembly, SettingsScope scope = SettingsScope.ApplicationScope ) : base( callingAssembly, scope )
        { }

        #region " TEST SITE CONFIGURATION "

        private bool _Verbose;

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( this.Verbose, value ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled;

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( this.Enabled, value ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _All;
        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public bool All
        {
            get => this._All;
            set {
                if ( !bool.Equals( this.All, value ) )
                {
                    this._All = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " TEST SITE LOCATION IDENTIFICATION "

        private string _TimeZones;


        /// <summary> Gets or sets the candidate time zones of the test site. </summary>
        /// <value> The candidate time zones of the test site. </value>
        public virtual string TimeZones
        {
            get => this._TimeZones;
            set {
                if ( !String.Equals( this.TimeZones, value ) )
                {
                    this._TimeZones = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.TimeZone ) );
                }
            }
        }

        private string _TimeZoneOffsets;


        /// <summary> Gets or sets the time zone offsets of the test sites. </summary>
        /// <value> The time zone offsets. </value>
        public virtual string TimeZoneOffsets
        {
            get => this._TimeZoneOffsets;
            set {
                if ( !String.Equals( this.TimeZoneOffsets, value ) )
                {
                    this._TimeZoneOffsets = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.TimeZoneOffset ) );
                }
            }
        }

        private string _IPv4Prefixes;
        /// <summary>   Gets or sets the IPv4 prefixes. </summary>
        /// <value> The IPv4 prefixes. </value>
        public virtual string IPv4Prefixes
        {
            get => this._IPv4Prefixes;
            set {
                if ( !String.Equals( this.IPv4Prefixes, value ) )
                {
                    this._IPv4Prefixes = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.HostInfoIndex ) );
                }
            }
        }

        private string _TimeZone;

        /// <summary> Gets the time zone of the test site. </summary>
        /// <value> The time zone of the test site. </value>
        public string TimeZone()
        {
            if ( string.IsNullOrWhiteSpace( this._TimeZone ) )
            {
                this._TimeZone = this.TimeZones.Split( ',' )[this.HostInfoIndex()];
            }
            return this._TimeZone;
        }

        /// <summary> The time zone offset. </summary>
        private double _TimeZoneOffset = double.MinValue;

        /// <summary> Gets the time zone offset of the test site. </summary>
        /// <value> The time zone offset of the test site. </value>
        public double TimeZoneOffset()
        {
            if ( this._TimeZoneOffset == double.MinValue )
            {
                this._TimeZoneOffset = double.Parse( this.TimeZoneOffsets.Split( ',' )[this.HostInfoIndex()] );
            }
            return this._TimeZoneOffset;
        }

        /// <summary> Gets the host name of the test site. </summary>
        /// <value> The host name of the test site. </value>
        public static string HostName()
        {
            return System.Net.Dns.GetHostName();
        }

        /// <summary> The host address. </summary>
        private System.Net.IPAddress _HostAddress;

        /// <summary> Gets the IP address of the test site. </summary>
        /// <value> The IP address of the test site. </value>
        public System.Net.IPAddress HostAddress()
        {
            if ( this._HostAddress is null )
            {
                foreach ( System.Net.IPAddress value in System.Net.Dns.GetHostEntry( HostName() ).AddressList )
                {
                    if ( value.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork )
                    {
                        this._HostAddress = value;
                        break;
                    }
                }
            }
            return this._HostAddress;
        }

        /// <summary> Zero-based index of the host information. </summary>
        private int _HostInfoIndex = -1;

        /// <summary> Gets the index into the host information strings. </summary>
        /// <value> The index into the host information strings. </value>
        public int HostInfoIndex()
        {
            if ( this._HostInfoIndex < 0 )
            {
                string ip = this.HostAddress().ToString();
                int i = -1;
                foreach ( string value in this.IPv4Prefixes.Split( ',' ) )
                {
                    i += 1;
                    if ( ip.StartsWith( value, StringComparison.OrdinalIgnoreCase ) )
                    {
                        this._HostInfoIndex = i;
                        break;
                    }
                }
            }

            return this._HostInfoIndex;
        }

        #endregion

    }
}
