
using isr.Json;
using isr.Modbus.Watlow.MSTest;

using System.Collections.Generic;
using System.Reflection;

namespace isr.Modbus.Watlow.MSTest
{

    /// <summary>   A RTU client settings. </summary>
    /// <remarks>   David, 2021-12-20. </remarks>
    [isr.Json.SettingsSection( nameof( RtuClientSettings ) )]
    public class RtuClientSettings : isr.Json.JsonSettingsBase
    {

        #region " CONSTRUCTOR "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        public RtuClientSettings() : base( System.Reflection.Assembly.GetAssembly( typeof( TestSiteSettings ) ) )
        { }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="callingAssembly">  The calling assembly. </param>
        /// <param name="scope">            (Optional) The scope. </param>
        protected RtuClientSettings( Assembly callingAssembly, SettingsScope scope = SettingsScope.ApplicationScope ) : base( callingAssembly, scope )
        { }

        #endregion

        #region " TEST SITE CONFIGURATION "

        private bool _Verbose;

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        public bool Verbose
        {
            get => this._Verbose;
            set {
                if ( !bool.Equals( this.Verbose, value ) )
                {
                    this._Verbose = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _Enabled = true;

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        public bool Enabled
        {
            get => this._Enabled;
            set {
                if ( !bool.Equals( this.Enabled, value ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private bool _All = true;
        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        public bool All
        {
            get => this._All;
            set {
                if ( !bool.Equals( this.All, value ) )
                {
                    this._All = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " RTU CLIENT INFORMATION "

        private string _FirstPortName = "COM3";
        /// <summary>   Gets or sets the name of the first port. </summary>
        /// <value> The name of the first port. </value>
        public string FirstPortName
        {
            get => this._FirstPortName;
            set {
                if ( !string.Equals( value, this.FirstPortName ) )
                {
                    this._FirstPortName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        private string _AlternatePortName = "COM4";
        /// <summary>   Gets or sets the name of the alternate port. </summary>
        /// <value> The name of the alternate port. </value>
        public string AlternatePortName
        {
            get => this._AlternatePortName;
            set {
                if ( !string.Equals( value, this.AlternatePortName ) )
                {
                    this._AlternatePortName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets the name of the port. </summary>
        /// <value> The name of the port. </value>
        public string PortName
        {
            get
            {
                var names = new List<string>( System.IO.Ports.SerialPort.GetPortNames() );
                return names.Contains( this.FirstPortName )
                    ? this.FirstPortName
                    : names.Contains( this.AlternatePortName )
                        ? this.AlternatePortName
                        : string.Empty;
            }
        }

        private byte _UnitId = 1;
        /// <summary>   Gets or sets the identifier of the unit. </summary>
        /// <value> The identifier of the unit. </value>
        public byte UnitId
        {
            get => this._UnitId;
            set {
                if ( !byte.Equals( value, this.UnitId ) )
                {
                    this._UnitId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _BaudRate = 38400;
        /// <summary>   Gets or sets the baud rate. </summary>
        /// <value> The baud rate. </value>
        public int BaudRate
        {
            get => this._BaudRate;
            set {
                if ( !int.Equals( value, this.BaudRate ) )
                {
                    this._BaudRate = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private int _AlarmType = 76;
        /// <summary>   Gets or sets the type of the alarm. </summary>
        /// <value> The type of the alarm. </value>
        public int AlarmType
        {
            get => this._AlarmType;
            set {
                if ( !int.Equals( value, this.AlarmType ) )
                {
                    this._AlarmType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " MEASUREMENTS "

        private float _AmbientTemperature = 72;
        /// <summary>   Gets or sets the ambient temperature. </summary>
        /// <value> The ambient temperature. </value>
        public float AmbientTemperature
        {
            get => this._AmbientTemperature;
            set {
                if ( !float.Equals( value, this.AmbientTemperature ) )
                {
                    this._AmbientTemperature = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private float _AmbientTemperatureEpsilon = 5;
        /// <summary>   Gets or sets the ambient temperature epsilon. </summary>
        /// <value> The ambient temperature epsilon. </value>
        public float AmbientTemperatureEpsilon
        {
            get => this._AmbientTemperatureEpsilon;
            set {
                if ( !float.Equals( value, this.AmbientTemperatureEpsilon ) )
                {
                    this._AmbientTemperatureEpsilon = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        #endregion

    }
}
