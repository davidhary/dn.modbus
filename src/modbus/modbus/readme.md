# About

isr.Modbus is a .Net library implementing the Modbus protocol.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Modbus is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Modbus Repository].

[Modbus Repository]: https://bitbucket.org/davidhary/dn.modbus

